//
//  MasterViewController.h
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

- (IBAction)processSampleImage;

@end

