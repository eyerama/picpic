//
//  SampleViewController.m
//  picpic
//
//  Created by jay on 2014. 10. 31..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "ImageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ImageWizard.h"
#import "MWKProgressIndicator.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation TiledScrollView

+ (Class)layerClass {
    return [CATiledLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        CATiledLayer *layer = (CATiledLayer *)self.layer;
        [layer setTileSize:CGSizeMake(100, 100)];
        [layer setLevelsOfDetailBias:2];
    }
    return self;
}

@end

@implementation TiledImageView

+ (Class)layerClass {
    return [CATiledLayer class];
}

- (id)initWithImage:(UIImage *)image {
    self = [super initWithImage:image];
    if(self) {
        CATiledLayer *tiledLayer = (CATiledLayer *)[self layer];
        tiledLayer.levelsOfDetail = 4;
        tiledLayer.levelsOfDetailBias = 3;
        tiledLayer.tileSize = CGSizeMake(512.0, 512.0);
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 5;
    }
    return self;
}

@end

@interface ImageViewController ()

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"DottedImage";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startImageProcess {
    [MWKProgressIndicator show];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        ALAssetsLibrary *lib = [ALAssetsLibrary new];
        [lib assetForURL:[NSURL URLWithString:self.imageUrl]
             resultBlock:^(__strong const ALAsset *asset) {
                 UIImage *image = [UIImage imageWithCGImage:[ImageWizard DottedImage:[[asset defaultRepresentation] fullScreenImage] dotSize:12 photoItemSize:20
                        progress:^(NSUInteger progress, NSUInteger total) {
                            NSLog(@"%ld / %ld", progress, total);
                            if (progress == total - 1) {
                                [MWKProgressIndicator dismiss];
                            }
                        }]];
                 self.imageView = [[UIImageView alloc] initWithImage:image];
                 [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
                 self.imageView.frame = self.scrollView.bounds;
                 [self.scrollView addSubview:self.imageView];
                 self.scrollView.contentSize = self.imageView.bounds.size;
             }
            failureBlock:^(NSError *error) {
            }];
    });
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
