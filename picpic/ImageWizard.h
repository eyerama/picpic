//
//  ImageWizard.h
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
#import "PicCollector.h"

@interface ColorSpace : NSObject

@property NSInteger r;
@property NSInteger g;
@property NSInteger b;
@property NSInteger greyscale; // 명암값

@end

@interface ImageWizard : NSObject

+ (ColorSpace *)calcAverageColorSpace:(CGImageRef)imageRef;
+ (CGImageRef)BurnedImage:(CGImageRef)image;
+ (CGImageRef)DottedImage:(CGImageRef)image
                  dotSize:(NSInteger)dotSize
            photoItemSize:(NSInteger)photoItemSize
                 progress:(ProgressBlock)progress;
+ (ColorSpace *)CalcAverageColorSpace:(UInt32 *)pixels
                                width:(NSUInteger)width
                                 rect:(CGRect)rect;
+ (UIImage*) fromImage:(UIImage*)source toColourR:(int)colR g:(int)colG b:(int)colB;

@end
