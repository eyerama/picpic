//
//  MasterViewController.m
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PicCollector.h"
#import "ImageWizard.h"
#import "MWKProgressIndicator.h"
#import "ImageViewController.h"

@interface MasterViewController ()

@property (nonatomic, strong) RLMResults *results;
@property (nonatomic, strong) RLMNotificationToken *token;
@property (nonatomic, strong) PicCollector *collector;
@property (nonatomic, assign) NSInteger progress;
@property (nonatomic, strong) NSTimer *timer;

- (void)processSampleImage;

@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MWKProgressIndicator updateMessage:@"ImageLoading"];
    self.collector = [PicCollector new];
    
    // Do any additional setup after loading the view, typically from a nib.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                               target:self
                                                                               action:@selector(reloadPics)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.results = [PicGroup allObjectsInRealm:[RLMRealm defaultRealm]];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(0 == self.results.count) {
        [self reloadPics];
    }
}

- (IBAction)processSampleImage {
    ImageViewController *svc = [[ImageViewController alloc] init];
    [self presentViewController:svc
                       animated:YES
                     completion:^{
                         
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadPics {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
    
    [self start];
}

- (void)start {
    
    [self.collector startPicCollect:^() {
        [MWKProgressIndicator show];
    }
                             finish:^() {
                                 [MWKProgressIndicator dismiss];
                             }
                           progress:^(CGFloat progress) {
                               NSLog(@"%f", progress);
                               [MWKProgressIndicator updateProgress:progress
                                                         completion:^{
                                                             
                                                         }];
                           }
                               sort:^{
                                   
                               }];
}

- (void)update:(NSString *)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MWKProgressIndicator updateMessage:progress];
    });
}

- (void)picLoadFinish {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Start Calculating");
        [MWKProgressIndicator updateMessage:@"Calculating"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self calcPics:[RLMRealm defaultRealm] gi:0 pi:0 count:0 time:CFAbsoluteTimeGetCurrent()];
            });
        });
    });
}

- (void)calcPics:(RLMRealm *)realm gi:(NSInteger)gi pi:(NSInteger)pi count:(NSInteger)count time:(CFAbsoluteTime)time {
    //
    //    if(nil == realm) {
    //        realm = [RLMRealm defaultRealm];
    //    }
    //    if(self.results.count <= gi) {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            NSLog(@"FinishCalculating");
    //            [MWKProgressIndicator dismiss];
    //            [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0]
    //                            withRowAnimation:UITableViewRowAnimationAutomatic];
    //        });
    //        return;
    //    }
    //    PicGroup *group = self.results[gi];
    //    Pic *pic = group.pics.count ? group.pics[pi] : nil;
    //    BOOL isNew = group.pics.count <= pi + 1;
    //    if(pic) {
    //        if( CFAbsoluteTimeGetCurrent() - 0.5 > time) {
    //            time = CFAbsoluteTimeGetCurrent();
    //            [MWKProgressIndicator updateMessage:[NSString stringWithFormat:@"Image Processing (%ld/%ld)", count, self.collector.totalCount]];
    //            [MWKProgressIndicator updateProgress:(float)count/self.collector.totalCount
    //                                      completion:^{
    //                                          ColorSpace *space = [ImageWizard calcAverageColorSpace:[UIImage imageWithData:pic.pic].CGImage];
    //                                          [realm beginWriteTransaction];
    //                                          pic.greyscale = space.greyscale;
    //                                          pic.r = space.r;
    //                                          pic.g = space.g;
    //                                          pic.b = space.b;
    //                                          [pic setSummary:[NSString stringWithFormat:@"R:%0.3ld G:%0.3ld B:%0.3ld G:%0.3ld", space.r, space.g, space.b, space.greyscale]];
    //                                          [realm commitWriteTransaction];
    //                                          [self calcPics:realm
    //                                                      gi:isNew ? gi + 1 : gi
    //                                                      pi:isNew ? 0 : pi + 1
    //                                                   count:count + 1
    //                                                    time:time];
    //                                      }];
    //        }
    //        else {
    //            ColorSpace *space = [ImageWizard calcAverageColorSpace:[UIImage imageWithData:pic.pic].CGImage];
    //            [realm beginWriteTransaction];
    //            pic.greyscale = space.greyscale;
    //            pic.r = space.r;
    //            pic.g = space.g;
    //            pic.b = space.b;
    //            [pic setSummary:[NSString stringWithFormat:@"R:%0.3ld G:%0.3ld B:%0.3ld G:%0.3ld", space.r, space.g, space.b, space.greyscale]];
    //            [realm commitWriteTransaction];
    //            [self calcPics:realm
    //                        gi:isNew ? gi + 1 : gi
    //                        pi:isNew ? 0 : pi + 1
    //                     count:count + 1
    //                      time:time];
    //        }
    //    }
    //    else {
    //        [self calcPics:realm
    //                    gi:isNew ? gi + 1 : gi
    //                    pi:isNew ? 0 : pi + 1
    //                 count:count + 1
    //                  time:time];
    //    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        PicGroup *picgroups = self.results[indexPath.row];
        RLMArray<Pic> *pics = picgroups.pics;
        controller.pics = (RLMArray<Pic> *)[pics sortedResultsUsingProperty:@"greyscale"
                                                                  ascending:NO];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //
    //    PicGroup *data = self.results[indexPath.row];
    //    cell.textLabel.text = [NSString stringWithFormat:@"%@(%ld)", data.name, data.count];
    //    cell.detailTextLabel.text = [data url];
    //    cell.imageView.image = [UIImage imageWithData:data.thumbnail scale:[UIScreen mainScreen].scale];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        PicGroup *group = self.results[indexPath.row];
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteObject:group];
        [realm commitWriteTransaction];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

@end
