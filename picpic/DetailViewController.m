//
//  DetailViewController.m
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "DetailViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImageViewController.h"

@interface DetailViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)configureView {
    // Update the user interface for the detail item.
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDatasource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section {
    return self.pics.count;
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"pic";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    Pic *pic = self.pics[indexPath.row];
    [cell.textLabel setAdjustsFontSizeToFitWidth:YES];
    cell.imageView.image = [UIImage imageWithData:pic.pic];
    cell.backgroundColor = [UIColor colorWithRed:pic.r / 255.0 green:pic.g / 255.0 blue:pic.b / 255.0 alpha:1.0];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([@"showimage" isEqualToString:[segue identifier]]) {
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        Pic *pic = self.pics[path.row];
        
        ImageViewController *ivc = [segue destinationViewController];
        [ivc setImageUrl:pic.url];
        
        [ivc performSelectorOnMainThread:@selector(startImageProcess)
                              withObject:nil
                           waitUntilDone:NO];
    }
}

@end
