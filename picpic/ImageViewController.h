//
//  SampleViewController.h
//  picpic
//
//  Created by jay on 2014. 10. 31..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TiledScrollView : UIScrollView

@end

@interface TiledImageView : UIImageView

@end

@interface ImageViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) NSString *imageUrl;

- (void)startImageProcess;

@end
