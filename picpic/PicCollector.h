//
//  Pic.h
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AssetsLibrary/AssetsLibrary.h>
#import <Realm/Realm.h>

typedef NS_ENUM(NSInteger, PicCollectionState) {
    NONE,
    START,
    FINISH,
    PROGRESS
};

@interface Pic : RLMObject

@property NSData *pic;
@property NSString *url;
@property NSInteger greyscale;

+ (instancetype)Pic:(ALAsset *)asset;

@end

RLM_ARRAY_TYPE(Pic)

@interface PicGroup : RLMObject

@property NSInteger greyscale;
@property NSInteger range;
@property RLMArray<Pic> *pics;

@end

RLM_ARRAY_TYPE(PicGroup)

@class PicCollector;

typedef void (^Block)();
typedef void (^StartBlock)();
typedef void (^EndBlock)();
typedef void (^ProgressBlock)(CGFloat progress);

void runOnMainThreadSynchronous(Block block);
void runOnPicQueueSynchronous(Block block);

@interface PicCollector : NSObject

@property (atomic, assign) NSInteger progressIndex;
@property (atomic, assign) NSInteger totalCount;
@property (atomic, assign) PicCollectionState state;
@property (nonatomic, strong) RLMResults *results;

+ (dispatch_queue_t)sharedQueue;
+ (const char *)sharedQueueLabel;
+ (dispatch_semaphore_t)sharedSemaphore;

- (void)startPicCollect:(StartBlock)start
                 finish:(EndBlock)end
               progress:(ProgressBlock)progress
                   sort:(void (^)(void))sort;

@end