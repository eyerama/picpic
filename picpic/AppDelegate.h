//
//  AppDelegate.h
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

