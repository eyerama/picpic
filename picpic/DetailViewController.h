//
//  DetailViewController.h
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
#import "PicCollector.h"

@interface DetailViewController : UITableViewController

@property (strong, nonatomic) RLMArray<Pic> *pics;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

