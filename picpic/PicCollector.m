//
//  Pic.m
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "PicCollector.h"
#import <CoreGraphics/CoreGraphics.h>
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
#import "ImageWizard.h"

void runOnMainThreadSynchronous(Block block) {
    if ([NSThread isMainThread]) {
        block();
    }
    else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            block();
        });
    }
}

void runOnMainThreadAsynchronous(Block block) {
    if ([NSThread isMainThread]) {
        block();
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            block();
        });
    }
}

void runOnPicQueueSynchronous(Block block) {
    if (dispatch_get_specific((const void *)[PicCollector sharedQueueLabel])) {
        block();
    }
    else {
        dispatch_sync([PicCollector sharedQueue], ^{
            block();
        });
    }
}

void runOnPicQueueAsynchronous(Block block) {
    if (dispatch_get_specific((const void *)[PicCollector sharedQueueLabel])) {
        NSLog(@"On pic queue asynchronous");
        block();
    }
    else {
        dispatch_async([PicCollector sharedQueue], ^{
            NSLog(@"On pic queue asynchronous");
            block();
        });
    }
}

@implementation Pic

+(instancetype)Pic:(ALAsset *)asset{
    CGImageRef imageRef = asset.thumbnail;
    CIImage *cimage = [CIImage imageWithCGImage:imageRef];
    CIFilter *filter = [CIFilter filterWithName:@"CIPhotoEffectTonal"];
    [filter setValue:cimage
              forKey:kCIAttributeTypeImage];
    CIImage *bwimage = [filter outputImage];
    
    Pic *pic = [Pic new];
    [pic setPic:UIImagePNGRepresentation([UIImage imageWithCIImage:bwimage])];
    [pic setUrl:[[asset valueForProperty:ALAssetPropertyAssetURL] absoluteString]];
    
    ColorSpace *space = [ImageWizard calcAverageColorSpace:imageRef];
    [pic setGreyscale:space.greyscale];
    
    return pic;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%0.3ld(G)", self.greyscale];
}
@end

@implementation PicGroup

- (NSString *)description {
    return [NSString stringWithFormat:@"%ld ~ %ld", self.greyscale, self.greyscale + self.range];
}

+(NSString *)primaryKey {
    return @"greyscale";
}

@end


@interface PicCollector ()

@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;

@end

@implementation PicCollector

+ (dispatch_queue_t)sharedQueue {
    static dispatch_queue_t queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create([PicCollector sharedQueueLabel], DISPATCH_QUEUE_CONCURRENT);
    });
    return queue;
}

+ (const char *)sharedQueueLabel {
    static const char* queue_label;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        queue_label = "com.jayapps.pic_queue";
    });
    return queue_label;
}

+ (dispatch_semaphore_t)sharedSemaphore {
    static dispatch_semaphore_t semaphore;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        semaphore = dispatch_semaphore_create(1);
    });
    return semaphore;
}

- (id)init {
    self = [super init];
    if (self) {
        [PicCollector sharedQueue];
        [PicCollector sharedSemaphore];
        self.results = [[PicGroup allObjects] sortedResultsUsingProperty:@"greyscaleFirst"
                                                               ascending:YES];
    }
    return self;
}

- (void)startPicCollect:(StartBlock)startCollectBlock
                 finish:(EndBlock)endCollectBlock
               progress:(ProgressBlock)progressBlock
                   sort:(void (^)(void))sort{
    
    startCollectBlock();
    __block RLMRealm *realm = [RLMRealm defaultRealm];
    runOnPicQueueAsynchronous(^{
        
        __block NSUInteger progress = 0;
        __block NSUInteger total = 0;
        ALAssetsLibrary *lib = [ALAssetsLibrary new];
        [lib enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                               if (group) {
                                   total += [group numberOfAssets];
                                   [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                                       if (result) {
                                           runOnMainThreadSynchronous(^{
                                               progress++;
                                               Pic *pic = [Pic Pic:result];
                                               [realm beginWriteTransaction];
                                               [realm addObject:pic];
                                               [realm commitWriteTransaction];
                                           });
                                           runOnMainThreadSynchronous(^{
                                               progressBlock(progress * .1 / total);
                                           });
                                       }
                                   }];
                               }
                               else {
                                   runOnPicQueueSynchronous(^{
                                       
                                       runOnMainThreadSynchronous(sort);
                                       
                                       runOnMainThreadSynchronous(^{
                                           RLMResults *pics = [[Pic allObjects] sortedResultsUsingProperty:@"greyscale"
                                                                                                 ascending:YES];
                                           for (Pic *pic in pics) {
                                               RLMResults *pgs = [PicGroup objectsWhere:@"(greyscale + range) < %ld", pic.greyscale];
                                               if (0 == pgs.count) { // 새로운 색 그룹을 생성
                                                   pg = [PicGroup new];
                                                   pg.greyscale = 0;
                                                   pg.range = pic.greyscale;
                                               }
                                           }
                                       });
                                       
                                       runOnMainThreadSynchronous(^{
                                           self.results = [Pic allObjects];
                                           endCollectBlock();
                                       });
                                   });
                               }
                           }
                         failureBlock:^(NSError *error) {
                             
                         }];
        
        
    });
}

@end