//
//  ImageWizard.m
//  picpic
//
//  Created by jay on 2014. 10. 30..
//  Copyright (c) 2014년 jay. All rights reserved.
//

#import "ImageWizard.h"
#import <Realm/Realm.h>
#import "PicCollector.h"
#import <CoreImage/CoreImage.h>

#define ByteMask(x) ((x) & 0xFF)
#define RMask(r) ( ByteMask(r) )
#define GMask(g) ( ByteMask(g >> 8) )
#define BMask(b) ( ByteMask(b >> 16) )
#define AMask(a) ( ByteMask(a >> 24) )
#define ColorMask(r,g,b) ( 0xFFFFFFFF & ( 255 << 24 | b << 16 | g << 8 | r ) )
#define Color2Grey(color) MIN( RMask(color) * 0.299 + GMask(color) * 0.587 + BMask(color) * 0.114, 255 )

@implementation ColorSpace

- (NSString *)description {
    return [NSString stringWithFormat:@"R:%0.3ld G:%0.3ld B:%0.3ld GREYSCALE:%ld", self.r, self.g, self.b, self.greyscale];
}

- (UInt32)color {
    return ColorMask(self.r, self.g, self.b);
}

@end

@implementation ImageWizard

+ (ColorSpace *)calcAverageColorSpace:(CGImageRef)imageRef {
    NSInteger width = CGImageGetWidth(imageRef);
    NSInteger height = CGImageGetHeight(imageRef);
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    
    UInt32 *pixels = (UInt32 *)calloc(height * width, sizeof(UInt32));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    ColorSpace *cs = [ImageWizard CalcAverageColorSpace:pixels
                                                  width:width
                                                   rect:CGRectMake(0, 0, width, height)];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(pixels);
    
    return cs;
}

+ (UInt32 *)ColorBurn:(NSInteger)height width:(NSInteger)width pixels:(UInt32 *)pixels pixelCount:(NSUInteger)pixelCount {
    UInt32 *newPixels = (UInt32 *)calloc(width * height, sizeof(UInt32));
    for (NSUInteger progressCount = 0 ; progressCount < pixelCount; progressCount ++) {
        UInt32 originalColor = pixels[progressCount];
        NSInteger r = RMask(originalColor);
        NSInteger g = GMask(originalColor);
        NSInteger b = BMask(originalColor);
        UInt32 *color = &newPixels[progressCount];
        if ( (r | g | b) < 128) { // is black
            *color = ColorMask(0, 0, 0);
            continue;
        }
        if(r > g && r > b) { // is red
            *color = ColorMask(255, 0, 0);
        }
        else if (g > r && g > b) { // is green
            *color = ColorMask(0, 255, 0);
        }
        else if (b > g && b > r) { // is blue
            *color = ColorMask(0, 0, 255);
        }
        else { // is grey
            NSInteger grey = Color2Grey(originalColor);
            if (grey < 128) { // is black
                newPixels[progressCount] = ColorMask(0, 0, 0);
            }
            else { // is white
                newPixels[progressCount] = ColorMask(255, 255, 255);
            }
        }
    }
    return newPixels;
}

+ (UInt32 *)ColorDot:(NSInteger)height width:(NSInteger)width pixels:(UInt32 *)pixels pixelCount:(NSUInteger)pixelCount dotSize:(NSInteger)dotSize {
    UInt32 *dottedPixels = (UInt32 *)calloc(width * height, sizeof(UInt32));
    for (NSUInteger progressCount = 0 ; progressCount < pixelCount; progressCount ++) {
    }
    return dottedPixels;
}

+ (CGImageRef)BurnedImage:(CGImageRef)image {
    NSInteger width = CGImageGetWidth(image);
    NSInteger height = CGImageGetHeight(image);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = 4 * width;
    NSUInteger bitsPerComponent = bytesPerPixel * 2;
    
    UInt32 *pixels = malloc(width * height * sizeof(UInt32));
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, bitsPerComponent, bytesPerRow, space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    
    NSUInteger pixelCount = width * height;
    UInt32 *burnedPixels = [self ColorBurn:height width:width pixels:pixels pixelCount:pixelCount];
    
    CGContextRelease(context);
    context = CGBitmapContextCreate(burnedPixels, width, height, bitsPerComponent, bytesPerRow, space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGImageRef dottedImageRef = CGBitmapContextCreateImage(context);
    CGColorSpaceRelease(space);
    free(pixels);
    free(burnedPixels);
    return dottedImageRef;
}

+ (Pic *)getPic:(NSInteger)greyscale results:(RLMResults *)results {
    RLMResults *exactPics = [results objectsWhere:@"greyscale == %ld", greyscale];
    RLMResults *mins = [[results objectsWhere:@"greyscale <= %ld", greyscale] sortedResultsUsingProperty:@"greyscale"
                                                                                               ascending:NO];
    RLMResults *maxs = [[results objectsWhere:@"greyscale >= %ld", greyscale] sortedResultsUsingProperty:@"greyscale"
                                                                                               ascending:YES];
    Pic *pic = nil;
    if(0 < exactPics.count) {
        pic = exactPics[arc4random_uniform((unsigned int)exactPics.count)];
    }
    else {
        if( 0 == ( mins.count & maxs.count ) ) {
            if ( 0 < mins.count ) {
                RLMResults *exactMaxs = [results objectsWhere:@"greyscale == %ld", [mins[0] greyscale]];
                pic = exactMaxs[arc4random_uniform((u_int32_t)exactMaxs.count)];
            }
            else if( 0 < maxs.count ) {
                RLMResults *exactMaxs = [results objectsWhere:@"greyscale == %ld", [maxs[0] greyscale]];
                pic = exactMaxs[arc4random_uniform((u_int32_t)exactMaxs.count)];
            }
            else {
                pic = nil;
            }
        }
        else {
            Pic *min = mins[0];
            Pic *max = maxs[0];
            if (greyscale - min.greyscale > max.greyscale - greyscale) {
                RLMResults *exactMaxs = [results objectsWhere:@"greyscale == %ld", max.greyscale];
                pic = exactMaxs[arc4random_uniform((u_int32_t)exactMaxs.count)];
            }
            else {
                RLMResults *exactMaxs = [results objectsWhere:@"greyscale == %ld", min.greyscale];
                pic = exactMaxs[arc4random_uniform((u_int32_t)exactMaxs.count)];
            }
        }
    }
    return pic;
}

+ (CGImageRef)DottedImage:(CGImageRef)image
                  dotSize:(NSInteger)dotSize
            photoItemSize:(NSInteger)photoItemSize
                 progress:(ProgressBlock)progress {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults *results = [Pic allObjectsInRealm:realm];
    
    // 스케일 범위 계산
    NSInteger minGrey = [results[0] greyscale];
    NSInteger maxGrey = [results[results.count-1] greyscale];
    
    NSInteger (^modify_greyscale_range)(NSInteger color) = ^NSInteger (NSInteger color) {
        return color * (maxGrey - minGrey - 2) / 255 + minGrey + 1;
    };
    
    NSInteger width = CGImageGetWidth(image) / dotSize;
    NSInteger height = CGImageGetHeight(image) / dotSize;
    
    NSInteger bitsPerComponent = 8;
    NSInteger bytesPerPixel = 4;
    NSInteger bytesPerRow = bytesPerPixel * width;
    
    UInt32 *pixels = (UInt32 *)calloc(width * height, sizeof(UInt32));
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, bitsPerComponent, bytesPerRow, space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    
    CGContextRelease(context);
    
    UInt8 *tempPixels = calloc(width * height * photoItemSize * photoItemSize, sizeof(UInt8));
    bytesPerRow = width * photoItemSize;
    CGColorSpaceRef graySpace = CGColorSpaceCreateDeviceGray();
    context = CGBitmapContextCreate(tempPixels, width * photoItemSize, height * photoItemSize, bitsPerComponent, bytesPerRow, graySpace, 0);
    NSUInteger total = width * height;
    for (NSInteger index = 0 ; index < total ; index ++) {
        NSUInteger x = index % width;
        NSUInteger y = height - index / width - 1;
        NSInteger greyColor = Color2Grey(pixels[index]);
//        if (minGrey > greyColor || maxGrey < greyColor) {
//            UInt8 fill = minGrey > greyColor ? 0 : 255;
//            for (NSInteger h = y ; h >= y + photoItemSize; h--) {
//                for (NSInteger w = x ; w >= x + photoItemSize ; w--) {
//                    NSUInteger pi = w + h * width * photoItemSize;
//                    tempPixels[pi] = fill;
//                }
//            }
//        }
//        else {
            Pic *pic = [ImageWizard getPic:modify_greyscale_range(greyColor)
                                   results:results];
            if (pic) {
                CGImageRef image = CGImageCreateWithPNGDataProvider(CGDataProviderCreateWithCFData((CFDataRef)pic.pic), NULL, NO, kCGRenderingIntentDefault);
                CGContextDrawImage(context, CGRectMake(x * photoItemSize, y * photoItemSize, photoItemSize, photoItemSize), image);
                
//            }
        }
        progress( index * .1 / total );
    }
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(space);
    free(pixels);
    return imageRef;
}

+ (void)FillAverageColor:(UInt32 *)src_pixels srcWidth:(NSInteger)srcWidth srcX:(NSInteger)srcX srcY:(NSInteger)srcY
                dstPixels:(UInt32 *)dst_pixels dstWidth:(NSInteger)dstWidth dstX:(NSInteger)dstX dstY:(NSInteger)dstY
                     size:(NSInteger)size {
    ColorSpace *cs = [ImageWizard CalcAverageColorSpace:src_pixels
                                                  width:srcWidth
                                                   rect:CGRectMake(srcX, srcY, size, size)];
    UInt32 color = cs.color;
    dst_pixels[dstX + dstY * dstWidth] = color;
}

+ (ColorSpace *)CalcAverageColorSpace:(UInt32 *)pixels
                                width:(NSUInteger)width
                                 rect:(CGRect)rect {
    
    NSUInteger pixelSize = rect.size.width * rect.size.height;
    ColorSpace *cs = [ColorSpace new];
    NSUInteger ar = 0;
    NSUInteger ag = 0;
    NSUInteger ab = 0;
    
    for (NSInteger y = 0 ; y < rect.size.width; y ++) {
        for(NSInteger x = 0 ; x < rect.size.height ; x ++ ) {
            UInt32 color = pixels[x + (NSUInteger)rect.origin.x + (y + (NSUInteger)rect.origin.y) * width];
            ar += RMask(color);
            ag += GMask(color);
            ab += BMask(color);
        }
    }
    
    cs.r = ar/(pixelSize);
    cs.g = ag/(pixelSize);
    cs.b = ab/(pixelSize);
    cs.greyscale = MIN(cs.r * 0.299 + cs.g * 0.587 + cs.b * 0.114, 255);
    return cs;
}

+ (ColorSpace *)CalcAverageColorSpace:(UInt32 *)pixels
                                width:(NSUInteger)width
                               origin:(CGPoint)point {
    
    ColorSpace *cs = [ColorSpace new];
    NSUInteger ar = 0;
    NSUInteger ag = 0;
    NSUInteger ab = 0;
    
    UInt32 color = pixels[(NSUInteger)(point.x + point.y * width)];
    ar = RMask(color);
    ag = GMask(color);
    ab = BMask(color);
    cs.greyscale = MIN(cs.r * 0.299 + cs.g * 0.587 + cs.b * 0.114, 255);
    return cs;
}

#define Clamp255(a) (a>255 ? 255 : a)

typedef unsigned char byte;

+ (UIImage*) fromImage:(UIImage*)source toColourR:(int)colR g:(int)colG b:(int)colB {
    
    // Thanks: http://brandontreb.com/image-manipulation-retrieving-and-updating-pixel-values-for-a-uiimage/
    CGContextRef ctx;
    CGImageRef imageRef = [source CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    byte *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    int byteIndex = 0;
    for (int ii = 0 ; ii < width * height ; ++ii)
    {
        int grey = (rawData[byteIndex] + rawData[byteIndex+1] + rawData[byteIndex+2]) / 3;
        
        rawData[byteIndex] = Clamp255(colR*grey/256);
        rawData[byteIndex+1] = Clamp255(colG*grey/256);
        rawData[byteIndex+2] = Clamp255(colB*grey/256);
        
        byteIndex += 4;
    }
    
    ctx = CGBitmapContextCreate(rawData,
                                CGImageGetWidth( imageRef ),
                                CGImageGetHeight( imageRef ),
                                8,
                                bytesPerRow,
                                colorSpace,
                                kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    imageRef = CGBitmapContextCreateImage (ctx);
    UIImage* rawImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    CGContextRelease(ctx);
    free(rawData);
    
    return rawImage;
}

@end
